module.exports = function(grunt) {
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-watch');

    grunt.initConfig({
        concat: {
            target: {
                files: {
                    'web/bundles/tymcore/js/core.js': ['web/bundles/tymcore/js/*.js', '!web/bundles/tymcore/js/*.min.js', '!web/bundles/tymcore/js/core.js'],
                }
            }
        },
        uglify: {
            target: {
                files: {
                    'web/bundles/tymcore/js/core.min.js': ['web/bundles/tymcore/js/core.js'],
                    'web/bundles/tymcore/js/lib/StackBlur.min.js': ['web/bundles/tymcore/js/lib/StackBlur.js']
                }
            }
        },
        cssmin: {
            target: {
                files: [
                    {
                      expand: true,
                      cwd: 'web/bundles/tymcore/css/',
                      src: ['**/*.css', '!**/*.min.css'],
                      dest: 'web/bundles/tymcore/css/',
                      ext: '.min.css',
                    }
                ]
            }
        },
        watch: {
            all: {
                files: ['web/bundles/tymcore/js/**/*.js', 'web/bundles/tymcore/css/*.css'],
                tasks: ['default'],
                options: {
                    spawn: false
                }
            }
        }
    });
    grunt.registerTask('default', ['concat', 'uglify', 'cssmin']);
    grunt.registerTask('watchAll', ['watch']);
};
