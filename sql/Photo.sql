-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Oct 24, 2013 at 11:39 PM
-- Server version: 5.5.25
-- PHP Version: 5.4.4

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `portfolio_symfony`
--

-- --------------------------------------------------------

--
-- Table structure for table `Photo`
--

CREATE TABLE `Photo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `url_picasa` varchar(255) NOT NULL,
  `url_medium` varchar(255) NOT NULL,
  `ordre` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `Photo`
--

INSERT INTO `Photo` (`id`, `url_picasa`, `url_medium`, `ordre`) VALUES
(3, 'https://picasaweb.google.com/lh/photo/ZR_3-tt6NAESHVH6geQKptMTjNZETYmyPJy0liipFm0?feat=directlink', 'img/pictures/pic1.jpg', 1),
(4, 'https://picasaweb.google.com/lh/photo/ehoiqq68mp4Wm3vntHrk09MTjNZETYmyPJy0liipFm0?feat=directlink', 'img/pictures/pic2.jpg', 2),
(6, 'https://picasaweb.google.com/lh/photo/HS8x_56BnMajKPRtL6i7M9MTjNZETYmyPJy0liipFm0?feat=directlink', 'img/pictures/pic3.jpg', 4),
(7, 'https://plus.google.com/photos/106761635340798801821/albums/5704900680616459521/5937212765717904658?banner=pwa&pid=5937212765717904658&oid=106761635340798801821', 'img/pictures/pic4.jpg', 3),
(9, 'https://plus.google.com/photos/106761635340798801821/albums/5704900680616459521/5937212779071910514?banner=pwa&pid=5937212779071910514&oid=106761635340798801821', 'img/pictures/pic5.jpg', 5),
(10, 'https://plus.google.com/photos/106761635340798801821/albums/5704900680616459521/5937212771616454866?banner=pwa&pid=5937212771616454866&oid=106761635340798801821', 'img/pictures/pic6.jpg', 6),
(11, 'https://plus.google.com/photos/106761635340798801821/albums/5704900680616459521/5937212809800756946?banner=pwa&pid=5937212809800756946&oid=106761635340798801821', 'img/pictures/pic7.jpg', 7);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
