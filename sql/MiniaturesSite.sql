-- phpMyAdmin SQL Dump
-- version OVH
-- https://www.phpmyadmin.net/
--
-- Hôte : theotimefolio.mysql.db
-- Généré le :  lun. 30 déc. 2019 à 22:48
-- Version du serveur :  5.6.42-log
-- Version de PHP :  7.2.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `theotimefolio`
--

-- --------------------------------------------------------

--
-- Structure de la table `MiniaturesSite`
--

CREATE TABLE `MiniaturesSite` (
  `id` int(11) NOT NULL,
  `site_id` int(11) NOT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ordre` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `MiniaturesSite`
--

INSERT INTO `MiniaturesSite` (`id`, `site_id`, `url`, `ordre`) VALUES
(1, 1, 'bestyear/vignettes/bestyear1.png', 1),
(2, 1, 'bestyear/vignettes/bestyear2.png', 2),
(3, 4, 'faircounts/vignettes/faircounts1.png', 1),
(4, 4, 'faircounts/vignettes/faircounts2.png', 2),
(5, 4, 'faircounts/vignettes/faircounts3.png', 3),
(6, 12, 'portfolio_v1/vignettes/portfolio1.png', 1),
(7, 12, 'portfolio_v1/vignettes/portfolio2.png', 2),
(8, 12, 'portfolio_v1/vignettes/portfolio3.png', 3),
(9, 1, 'bestyear/vignettes/bestyear3.png', 3),
(10, 14, 'mynewsdesigner/vignettes/mnd2.png', 1),
(11, 14, 'mynewsdesigner/vignettes/mnd3.png', 2),
(12, 14, 'mynewsdesigner/vignettes/mnd4.png', 3);

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `MiniaturesSite`
--
ALTER TABLE `MiniaturesSite`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_4637E721F6BD1646` (`site_id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `MiniaturesSite`
--
ALTER TABLE `MiniaturesSite`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `MiniaturesSite`
--
ALTER TABLE `MiniaturesSite`
  ADD CONSTRAINT `FK_4637E721F6BD1646` FOREIGN KEY (`site_id`) REFERENCES `Site` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
