-- phpMyAdmin SQL Dump
-- version OVH
-- https://www.phpmyadmin.net/
--
-- Hôte : theotimefolio.mysql.db
-- Généré le :  lun. 30 déc. 2019 à 22:51
-- Version du serveur :  5.6.42-log
-- Version de PHP :  7.2.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `theotimefolio`
--

-- --------------------------------------------------------

--
-- Structure de la table `Site`
--

CREATE TABLE `Site` (
  `id` int(11) NOT NULL,
  `url_preview` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` longtext NOT NULL,
  `url` varchar(255) NOT NULL,
  `year` int(11) NOT NULL,
  `technologies` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `url_preview_large` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `Site`
--

INSERT INTO `Site` (`id`, `url_preview`, `title`, `description`, `url`, `year`, `technologies`, `status`, `url_preview_large`) VALUES
(1, 'bestyear/bestyear1-2.png', 'Bestyear UTC', '<p>Le Bestyear est un annuaire dédié aux étudiants de l\'UTC. Il permet de retrouver facilement des informations comme le numéro de téléphone ou l\'adresse email des étudiants (à condition qu\'ils soient inscris).</p>\r\n\r\n<p>Mon projet ici est de relancer cette association en proposant un nouveau service avec une interface \"Flat Design\" plus agréable. Un accent a été mis pour que le site soit relativement interactif grâce à JQuery. Ainsi, les pages principales \"glissent\" sans rechargement, en utilisant une liste d\'icônes s\'affichant sur la barre latérale. Par ailleurs, les formulaires sont validés une première fois par JQuery pour avoir un feedback immédiat, puis vérifiés par PHP grâce à Symfony 2.</p>', '', 2013, 'HTML, CSS3, JQuery, Symfony 2', 'En cours', 'bestyear/bestyear1.png'),
(2, 'homepage/homepage.png', 'Homepage', '<p>Cette page me sert actuellement de page d’accueil. Elle a subi quelques améliorations progressives, principalement au niveau du design de la page. On y retrouve des liens vers les sites que je consulte le plus souvent, pour mon divertissement comme pour mes études. De plus, j’ai ajouté une barre de recherche Google permettant de faire une recherche rapide.</p>\r\n\r\n<p>Ce site m’a surtout permis d’utiliser du CSS3 pour la mise en page. J’ai ainsi mis des effets au survol de la souris, des effets sur les ombres… Cela rend la page dynamique et assez agréable à utiliser.</p>', '', 2010, 'HTML, CSS3', 'Terminé', 'homepage/homepage1.png'),
(4, 'faircounts/faircounts.png', 'FairCounts', '<p>\r\nCe projet effectué dans le cadre du cours CSC 305 à l\'Université de Rhode Island est une web app qui vise à faciliter les dépenses de groupe (voyage entre amis, organisation de soirée, ...). Le principe est relativement simple: chacun entre les dépenses qu\'il a fait et les personnes qui devront rembourser. Au fur et à mesure que les dépenses sont ajoutées, le solde total varie et chaque utilisateur peut voir s\'il doit de l\'argent à quelqu\'un ou s\'il va être remboursé. \r\n</p>\r\n<p>\r\nL\'accent est mis sur les documents à fournir dans le cadre du management de projets. \"Requirements\", \"Design Document\", \"Budget\", tous ces documents sont rédigés (en anglais) afin de simuler les contraintes imposées par les clients permettant d\'obtenir une certaine rigueur sur le travail qui est réalisé.\r\n</p>\r\n<p>\r\nD\'un point de vue technique, l\'objectif est de créer un site utilisant peu de pages à proprement parler afin que le tout soit fluide et interactif. Le travail est divisé en deux équipes de deux personnes, une sur la partie Backend en Symfony 2 et une autre équipe sur le FrontEnd (dont je fais partie). \r\n</p>', '', 2013, 'HTML, CSS, AngularJS, PHP, Symfony 2', 'Terminé', 'faircounts/faircounts1.png'),
(8, 'memorae/memorae.png', 'MEMORAe', '<p>Ce projet s\'inscrit dans le cadre d\'une TX (cours enseigné à l\'UTC permettant d\'accomplir un projet original, défini par un professeur, généralement dans le cadre de ses recherches). L\'objectif était d\'améliorer l\'interface utilisateur tout en ajoutant la possibilité de se connecter à une nouvelle API.\r\n</p>\r\n<p> Le projet en lui-même est un logiciel de brainstorming utilisant des \"notes\" (Post-it) et des \"clusters\" (groupe de Post-it) pour partager des idées. L\'application tourne sur table tactile ainsi que sur ordinateur. Elle utilise Java, MT4J et JADE pour l\'aspect multi-agent, permettant une synchronisation entre plusieurs appareils situés à des lieux différents. \r\n</p>\r\n<p>L\'une des premières tâches a été de redesigner les notes tout en gardant la possibilité d\'utiliser à la fois la souris ou les gestes multitouch pour interagir. Ainsi, compte-tenu des limites de MT4J sur la gestion des images, j\'ai utilisé des paradigmes déjà présents dans les outils de traitement de texte (une bulle à déplacer pour faire une rotation, un triangle constitué de trois lignes pour le redimensionnement).\r\n</p>\r\n<p>\r\nJ\'ai également ajouté quelques fonctionnalités comme la possibilité de choisir la couleur d\'une note, changer la taille de la police d\'écriture et sa couleur ainsi que la couleur par défaut d\'un utilisateur. Cela se répercute instantanément sur les autres machines connectées au même brainstorming grâce à l\'envoi de message.\r\n</p>\r\n<p>\r\nLa seconde partie du projet consistait à adapter le code à l\'utilisation d\'une nouvelle API. Il s\'agissait donc d\'adapter le comportement de l\'application à une API différente sans pour autant modifier le coeur du programme. Cela se fait en modifiant certains messages et certaines classes pour que le JSON soit parsé automatiquement.\r\n</p>', 'http://www.hds.utc.fr/memorae/?lang=en', 2013, 'Java, MT4J', 'Terminé', 'memorae/memorae1.png'),
(12, 'portfolio_v1/portfolio.png', 'Portfolio v1', '<p>\r\nLe portfolio sur lequel vous vous trouvez a connu une version précédente. Plus sombre, plus austère et avec quelques problèmes de conceptions il contenait approximativement les mêmes informations. Il m\'a permis de me familiariser avec le framework Symfony 2 tout en travaillant un peu avec le design et quelques effets CSS3. J\'ai également tenter d\'ajouter quelques effets en JavaScript ensuite transformés en JQuery. La page principale faisait coulisser des \"panneaux\", un peu comme la version actuelle, pour afficher une présentation, des liens vers la galerie photo et la liste des projets, un biographie et un formulaire de contact.\r\n</p>\r\n<p>\r\nJ\'ai choisi de le remplacer par une version plus moderne, lisible sur mobile et tablettes (sur le principe du responsive design) et reflétant les techniques que j\'ai acquises. Le nouveau portfolio adopte un \"flat\" design inspiré des cartes présentes dans Google Now. Le tout est accessible sur une seule page, évitant les temps de chargement.\r\n</p>', '', 2012, 'HTML, CSS3, PHP, Symfony2', 'Supprimé', 'portfolio_v1/portfolio1.png'),
(13, 'okiya/okiya.jpg', 'Okiya', '<p>\r\nOkiya est le nom d\'un jeu de plateau sur lequel nous avons travaillé dans le cadre de l\'UV IA02 enseignée à l\'UTC. Pour ce projet, nous avons dû créer un programme doté d\'intelligence pouvant jouer au jeu d\'Okiya. Plusieurs modes étaient proposés: Humain vs Humain, Humain vs IA ou IA vs IA. L\'interface était une simple ligne de commande affichant le plateau ainsi que les coups joués. \r\n</p>\r\n<p>\r\nL\'intelligence artificielle évalue un grand nombre de combinaisons possibles et les répercutions que les choix auront sur la suite de la partie pour déterminer quel est le meilleur coup à jouer. Comme les ressources des machines sont limitées, l\'IA ne cherche pas à évaluer toutes les combinaisons existantes et se limite à une profondeur de 3 dans l\'arbre de recherche. Au final, l\'IA n\'est pas imbattable mais réagit suffisamment bien pour faire quelques parties sans comportement menant clairement à une défaite.\r\n</p>', 'https://bitbucket.org/tym_network/ia02-projet/overview', 2013, 'Prolog', 'Terminé', ''),
(14, 'mynewsdesigner/mynewsdesigner.png', 'MyNewsDesigner', '<p>MyNewsDesigner est une application pour tablettes Android qui permet de créer un magazine à partir d\'articles provenant d\'Internet. Elle a été réalisée dans le cadre de l\'UV NF28 \"Ingénierie des systèmes intéractifs\". Pour ce projet j\'étais en charge de la création d\'une grande partie des éléments de design ainsi que de la sélection des articles.</p>\r\n\r\n<p>Le but de ce projet était de mettre en avant l\'intéraction homme-machine afin d\'être le plus simple possible tout en proposant un certain nombre de fonctionnalités. Pour ce faire, nous avons essayé de limiter la quantité de texte sur l\'interface en privilégiant l\'utilisation d\'icônes étant donné que le contenu en lui-même était déjà très textuel. D\'autre part, nous avons essayé de mettre en avant des gestes (étant donné que l\'on travail sur des tablettes multitouch) pour effectuer des actions. Par exemple, pour supprimer un magazine d\'une liste, il suffit de \"swiper\" le magazine en dehors du cadre.</p>\r\n', '', 2013, 'Java, Android, RSS', 'Terminé', '');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `Site`
--
ALTER TABLE `Site`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `Site`
--
ALTER TABLE `Site`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
