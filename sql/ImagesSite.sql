-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Oct 25, 2013 at 01:36 AM
-- Server version: 5.5.25
-- PHP Version: 5.4.4

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `portfolio_symfony`
--

-- --------------------------------------------------------

--
-- Table structure for table `ImagesSite`
--

CREATE TABLE `ImagesSite` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `site_id` int(11) NOT NULL,
  `urlImg` varchar(255) NOT NULL,
  `urlBig` varchar(255) NOT NULL,
  `ordre` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_70278D91F6BD1646` (`site_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `ImagesSite`
--

INSERT INTO `ImagesSite` (`id`, `site_id`, `urlImg`, `urlBig`, `ordre`) VALUES
(2, 2, 'images/sites/homepage.jpg', 'images/sites/Big/homepage.jpg', 1);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `ImagesSite`
--
ALTER TABLE `ImagesSite`
  ADD CONSTRAINT `FK_70278D91F6BD1646` FOREIGN KEY (`site_id`) REFERENCES `Site` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
