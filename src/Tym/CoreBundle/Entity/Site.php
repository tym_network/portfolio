<?php
namespace Tym\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Tym\CoreBundle\Entity\SiteRepository")
 */
class Site
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string $url_preview
     *
     * @ORM\Column(name="url_preview", type="string", length=255)
     */
    private $url_preview;
    
    /**
     * @var string $url_preview_large
     *
     * @ORM\Column(name="url_preview_large", type="string", length=255)
     */
    private $url_preview_large;

    /**
     * @var string $title
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;

    /**
     * @var text $description
     *
     * @ORM\Column(name="description", type="text")
     */
    private $description;

    /**
     * @var string $url
     *
     * @ORM\Column(name="url", type="string", length=255)
     */
    private $url;

    /**
     * @var integer $year
     *
     * @ORM\Column(name="year", type="integer")
     */
    private $year;
    
    /**
     * @var string $technologies
     *
     * @ORM\Column(name="technologies", type="string", length=255)
     */
    private $technologies;
    
    /**
     * @var string $status
     *
     * @ORM\Column(name="status", type="string", length=255)
     */
    private $status;

    /**
     * @ORM\OneToMany(targetEntity="Tym\CoreBundle\Entity\ImagesSite", mappedBy="site")
     */
    private $images;
    
    /**
     * @ORM\OneToMany(targetEntity="Tym\CoreBundle\Entity\MiniaturesSite", mappedBy="site")
     */
    private $miniatures;
    
    public function __construct()
    {
        $this->images = new \Doctrine\Common\Collections\ArrayCollection();
        $this->miniatures = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set url_preview
     *
     * @param string $urlpreview
     */
    public function setUrlPreview($urlPreview)
    {
        $this->url_preview = $urlPreview;
    }

    /**
     * Get url_preview
     *
     * @return string 
     */
    public function getUrlPreview()
    {
        return $this->url_preview;
    }
    
    /**
     * Set url_preview_large
     *
     * @param string $urlPreviewLarge
     */
    public function setUrlPreviewLarge($urlPreviewLarge)
    {
        $this->url_preview_large = $urlPreviewLarge;
    }

    /**
     * Get url_preview_large
     *
     * @return string 
     */
    public function getUrlPreviewLarge()
    {
        return $this->url_preview_large;
    }

    /**
     * Set title
     *
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set description
     *
     * @param text $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * Get description
     *
     * @return text 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set url
     *
     * @param string $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }

    /**
     * Get url
     *
     * @return string 
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set year
     *
     * @param integer $year
     */
    public function setYear($year)
    {
        $this->year = $year;
    }

    /**
     * Get year
     *
     * @return integer 
     */
    public function getYear()
    {
        return $this->year;
    }
    
    /**
     * Set technologies
     *
     * @param string $technologies
     */
    public function setTechnologies($technologies)
    {
        $this->technologies = $technologies;
    }

    /**
     * Get technologies
     *
     * @return integer 
     */
    public function getTechnologies()
    {
        return $this->technologies;
    }
    
    /**
     * Set status
     *
     * @param string $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * Get status
     *
     * @return string 
     */
    public function getStatus()
    {
        return $this->status;
    }
    
    /**
     * Get imagessite
     *
     * @return ArrayCollection 
     */
    public function getImagessite()
    {
        return $this->images;
    }
     
    /**
     * Get miniatures
     *
     * @return ArrayCollection 
     */
    public function getMiniatures()
    {
        return $this->miniatures;
    }
}