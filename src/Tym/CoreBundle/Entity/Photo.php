<?php

namespace Tym\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Tym\CoreBundle\Entity\Photo
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Tym\CoreBundle\Entity\PhotoRepository")
 */
class Photo
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string $url_picasa
     *
     * @ORM\Column(name="url_picasa", type="string", length=255)
     */
    private $url_picasa;

    /**
     * @var string $url_medium
     *
     * @ORM\Column(name="url_medium", type="string", length=255)
     */
    private $url_medium;
    
    /**
     * @var integer $ordre
     *
     * @ORM\Column(name="ordre", type="integer")
     */
    private $ordre;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set url_picasa
     *
     * @param string $urlPicasa
     */
    public function setUrlPicasa($urlPicasa)
    {
        $this->url_picasa = $urlPicasa;
    }

    /**
     * Get url_picasa
     *
     * @return string 
     */
    public function getUrlPicasa()
    {
        return $this->url_picasa;
    }

    /**
     * Set url_medium
     *
     * @param string $urlMedium
     */
    public function setUrlMedium($urlMedium)
    {
        $this->url_medium = $urlMedium;
    }

    /**
     * Get url_medium
     *
     * @return string 
     */
    public function getUrlMedium()
    {
        return $this->url_medium;
    }

    /**
     * Set ordre
     *
     * @param integer $ordre
     */
    public function setOrdre($ordre)
    {
        $this->ordre = $ordre;
    }

    /**
     * Get ordre
     *
     * @return integer 
     */
    public function getOrdre()
    {
        return $this->ordre;
    }
}