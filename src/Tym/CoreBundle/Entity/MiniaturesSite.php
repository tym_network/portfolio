<?php

namespace Tym\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * MiniaturesSite
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class MiniaturesSite
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="url", type="string", length=255)
     */
    private $url;

    /**
     * @var integer
     *
     * @ORM\Column(name="ordre", type="integer")
     */
    private $ordre;

    /**
    * @ORM\ManyToOne(targetEntity = "Tym\CoreBundle\Entity\Site", inversedBy="miniatures")   
	* @ORM\JoinColumn(nullable=false)
	*/
	private $site;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set url
     *
     * @param string $url
     * @return MiniaturesSite
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set ordre
     *
     * @param integer $ordre
     * @return MiniaturesSite
     */
    public function setOrdre($ordre)
    {
        $this->ordre = $ordre;

        return $this;
    }

    /**
     * Get ordre
     *
     * @return integer
     */
    public function getOrdre()
    {
        return $this->ordre;
    }

    /**
     * Set site
     *
     * @param Tym\CoreBundle\Entity\Site $site
     */
    public function setSite(Tym\CoreBundle\Entity\Site $site)
    {
        $this->site = $site;
    }

    /**
     * Get site
     *
     * @return Tym\CoreBundle\Entity\Site
     */
    public function getSite()
    {
        return $this->site;
    }
}
