<?php

namespace Tym\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Tym\CoreBundle\Entity\ImagesSite
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Tym\CoreBundle\Entity\ImagesSiteRepository")
 */
class ImagesSite
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string $url
     *
     * @ORM\Column(name="url", type="string", length=255)
     */
    private $url;

    /**
     * @var integer $ordre
     *
     * @ORM\Column(name="ordre", type="integer")
     */
    private $ordre;

    /**
    * @ORM\ManyToOne(targetEntity = "Tym\CoreBundle\Entity\Site", inversedBy="images")
    * @ORM\JoinColumn(nullable=false)
    */
    private $site;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set url
     *
     * @param string $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }

    /**
     * Get url
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set ordre
     *
     * @param integer $ordre
     */
    public function setOrdre($ordre)
    {
        $this->ordre = $ordre;
    }

    /**
     * Get ordre
     *
     * @return integer
     */
    public function getOrdre()
    {
        return $this->ordre;
    }

    /**
     * Set site
     *
     * @param Tym\CoreBundle\Entity\Site $site
     */
    public function setSite(Tym\CoreBundle\Entity\Site $site)
    {
        $this->site = $site;
    }

    /**
     * Get site
     *
     * @return Tym\CoreBundle\Entity\Site
     */
    public function getSite()
    {
        return $this->site;
    }
}
