<?php

namespace Tym\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use EWZ\Bundle\RecaptchaBundle\Validator\Constraints as Recaptcha;

/**
 * Tym\CoreBundle\Entity\Message
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Tym\CoreBundle\Entity\MessageRepository")
 */
class Message
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string $nom
     *
     * @ORM\Column(name="nom", type="string", length=255, nullable=false)
     * @Assert\NotBlank(message = "Champ vide")
     * @Assert\Regex(pattern = "# #",message = "Entrez votre nom et votre prénom séparés par un espace")
     */
    private $nom;

    /**
     * @var string $mail
     *
     * @ORM\Column(name="mail", type="string", length=255, nullable=false)
     * @Assert\NotBlank(message = "Champ obligatoire")
     * @Assert\Email(
     *     message = "L'adresse e-mail '{{ value }}' n'est pas une adresse valide."
     * )
     */
    private $mail;

    /**
     * @var text $contenu
     *
     * @ORM\Column(name="contenu", type="text", nullable=false)
     * @Assert\NotBlank(message = "Jean-Paul Sartres a dit 'La douleur c'est le vide', sachez qu'avec ce message vide, vous me touchez en plein coeur.")
     */
    private $contenu;

    /**
     * @var datetime $date
     *
     * @ORM\Column(name="date", type="datetime", nullable=false)
     * @Assert\NotBlank()
     */
    private $date;

    /**
    * @Recaptcha\IsTrue
    */
    public $recaptcha;

    public function __construct()
    {
        $this->date = new \Datetime();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     */
    public function setNom($nom)
    {
        $this->nom = $nom;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set mail
     *
     * @param string $mail
     */
    public function setMail($mail)
    {
        $this->mail = $mail;
    }

    /**
     * Get mail
     *
     * @return string
     */
    public function getMail()
    {
        return $this->mail;
    }

    /**
     * Set contenu
     *
     * @param text $contenu
     */
    public function setContenu($contenu)
    {
        $this->contenu = $contenu;
    }

    /**
     * Get contenu
     *
     * @return text
     */
    public function getContenu()
    {
        return $this->contenu;
    }

    /**
     * Set date
     *
     * @param datetime $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    /**
     * Get date
     *
     * @return datetime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set recaptcha
     *
     * @param text $recaptcha
     */
    public function setRecaptcha($value)
    {
        $this->recaptcha = $value;
    }

    /**
     * Get recaptcha
     *
     * @return recaptcha
     */
    public function getRecaptcha()
    {
        return $this->recaptcha;
    }
}