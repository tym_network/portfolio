(function($) {
    var currentPageShown = 1,
        pageMatrix = ['#home', '#projects', '#photos', '#biography', '#contact'];

    $(window).load(function () {
        $('#links').css('height', '0');
        displayCorrectPage();
        correctCanvasHeight();
    });

    /**
     * When the window is resized, adapt the height of the content.
     */
    $(window).resize(function () {
        if (currentPageShown === 2) {
            $.resizeAction();
        }

        var currentPage = $('#page'+currentPageShown);
        setPageHeight(currentPage.css('height'));
        correctCanvasHeight();
    });

    /**
     * Change the height of the canvas to fit the picture.
     */
    var correctCanvasHeight = function() {
        $('#photoViewer').css('height', $('#pic1').css('height'));
        $('.canvas').css({
            'width': $('#photoViewer').css('width'),
            'height': 'auto',
        });
    };

    /**
     * Set the size of the page so that is covers at least the whole page.
     */
    var setPageHeight = function(h) {
        var pannelView = $('#pannelView');
        var min = $(window).height()-($('#footer').height()+$('#header').height()+parseInt($('#footer').css('marginTop').replace('px',''))+parseInt($('#footer').css('marginBottom').replace('px','')));
        if (h.replace('px','') > min) {
            pannelView.css('height', h);
        } else {
            pannelView.css('height', min+'px');
        }
    };

    /**
     * Redirect to the right page depending on the hash tag.
     */
    var displayCorrectPage = function() {
        var pageAsked = jQuery.inArray($(location).attr('hash'), pageMatrix)+1;
        if (!pageAsked) {
            pageAsked = 1;
        }
        initiate(pageAsked);
    };

    /**
     * Display the page with the id number 'id'
     */
    var moveToContent = function(id) {
        if (id != currentPageShown) {
            var $currentPage = $('#page'+currentPageShown),
                $newPage = $('#page'+id);

            $newPage.css('transition', '');
            if (id > currentPageShown) {
                // Display a page that is after the current one
                $currentPage.css('transition', 'transform .6s cubic-bezier(1,0,0,1)');
                $currentPage.css('transform', 'translateX(-100vw)');
                $newPage.css('transform', 'translateX(100vw)');
            } else if (id < currentPageShown) {
                // Display a page that is before the current one
                $currentPage.css('transition', 'transform .6s cubic-bezier(1,0,0,1)');
                $currentPage.css('transform', 'translateX(100vw)');
                $newPage.css('transform', 'translateX(-100vw)');
            }
            setTimeout(function() {
                $newPage.css('transition', 'transform .6s cubic-bezier(1,0,0,1)');
                $newPage.css('transform', 'translateX(0)');
            });
            $('#links li').eq(id-1).addClass('current');
            $('#links li').eq(currentPageShown-1).removeClass('current');
            setPageHeight($newPage.css('height'));

            setKeyboardShortcuts(currentPageShown, id);

            currentPageShown = id;
            ga('send', 'pageview', {
              'page': pageMatrix[id],
              'title': pageMatrix[id]
            });
        }
    };

    /**
     * Use mousetrap to set keyboard shortcuts (switch between pictures)
     *
     * @param {integer} previousId - ID of the page that will be hidden
     * @param {integer} nextId - ID of the page that will be shown
     */
    var setKeyboardShortcuts = function(previousId, nextId) {
        // Remove Listeners for projects and pictures
        if (previousId === 2 || previousId === 3) {
            Mousetrap.unbind('right');
            Mousetrap.unbind('left');
        }
        if (nextId === 2) {
            // For projects
            Mousetrap.bind('right', function() { $.nextProjectPicture(); });
            Mousetrap.bind('left', function() { $.previousProjectPicture(); });
        }
        else if (nextId === 3) {
            // For pictures
            Mousetrap.bind('right', function() { $.nextPicture(); });
            Mousetrap.bind('left', function() { $.previousPicture(); });
        }
    };

    var initiate = function(page) {
        var $newPage = $('#page' + page);
        currentPageShown = page;
        $newPage.css('transform', 'translateX(0)');
        $('#links li').eq(page-1).addClass('current');
        setPageHeight($newPage.css('height'));
    };

    $('#links a').click(function() {
        linkClicked($(this).parent().index()+1);
    });

    $('#logo').click(function() {
        linkClicked(1);
    });

    $('#siteTitle').click(function() {
        linkClicked(1);
    });

    var linkClicked = function(id) {
        moveToContent(id);
        if (navIsShown) {
            $('#links').css('height', '0');
            navIsShown = false;
        }
    };

    /**
     * For the nav in Smartphone mode
     */

    var navIsShown = false;

    $('#menuButtonSmartphone').click(function() {
        if (navIsShown) {
            $('#links').css('height', '0');
            navIsShown = false;
        } else {
            $('#links').css('height', '');
            navIsShown = true;
        }
    });


    /**
     * Hide heaband when clicking on close button
     */
    var $headbandClose = $('.headband-close');
    $headbandClose.click(function() {
        $('.headband').remove();

        // Set cookie
        document.cookie = 'hide-banner=1; path=/';
    });
})(jQuery);

function initialize() {
    var map_canvas = document.getElementById('map_canvas');
    var map_options = {
        center: new google.maps.LatLng(48.8373, 2.2413),
        zoom: 13,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    }
    var map = new google.maps.Map(map_canvas, map_options);
    var latLng = new google.maps.LatLng(48.8373, 2.2413);
    var marker = new google.maps.Marker({
        position: latLng,
        map: map,
    });
}

if (google && google.maps) {
    google.maps.event.addDomListener(window, 'load', initialize);
}
(function($) {
    'use strict';
    var totPictures    = $(".picture").length,
        currentPhotoId = 1,
        previousId;

    /**
     * Fill the canvas with a blurry version of the pictures and display the first picture
     */
    var initialize = function() {
        var $pictures = $(".picture"),
            $canvas = $(".canvas"),
            $zoom = $(".zoom");

        // Display first picture
        $pictures.first().css({
          "opacity": "1",
          "display": "block"
        });
        $canvas.first().css("opacity", 1);
        $zoom.first().css("display", "block");
        // Add blurred canvas
        $pictures.each(function() {
            var num = $(this).attr("id").replace("pic", ""),
                canvas = document.getElementById("blurPic"+num),
                picture = this;

            canvas.getContext("2d").drawImage(picture, 0, 0);
            stackBlurImage($(this).attr("id"), "blurPic"+num, 40, false);
        });
    };

    /**
     * Display a specific picture
     */
    var showPicture = function(id) {
        previousId = currentPhotoId;
        currentPhotoId = id;
        var $oldPicture = $("#pic"+previousId),
            $oldZoom = $("#zoom"+previousId),
            $otherCanvas = $(".canvas"),
            $oldRound = $("#round"+previousId),
            $newPicture = $("#pic"+currentPhotoId),
            $newZoom = $("#zoom"+currentPhotoId),
            $newCanvas = $("#blurPic"+currentPhotoId),
            $newRound = $("#round"+currentPhotoId);

        $oldPicture.animate({"opacity": "0"}, 500, "linear", function() {
            $oldZoom.css("display", "");
            if (currentPhotoId==id) {
                $newZoom.css("display", "block");
                $newPicture.animate({"opacity": "1"}, 1000, "linear", function() {
                    $otherCanvas.css("opacity", 0);
                    $newCanvas.css("opacity", 1);
                });
            }
        });
        $oldRound.removeClass("selected");
        $newRound.addClass("selected");
    };

    /**
     * Display the next picture
     */
    var nextPicture = function() {
        var nextPhotoId = (currentPhotoId === totPictures) ? 1 : currentPhotoId+1;
        showPicture(nextPhotoId);
    };

    /**
     * Display the previous picture
     */
    var previousPicture = function() {
        var previousPhotoId = (currentPhotoId === 1) ? totPictures : currentPhotoId-1;
        showPicture(previousPhotoId);
    };

    $(window).load(function() {
        initialize();
    });

    $("#rightArrow").click(function() {
        nextPicture();
    });

    $("#leftArrow").click(function() {
        previousPicture();
    });

    $(".photos .circleButton").click(function() {
        showPicture(parseInt($(this).attr("id").replace("round","")));
    });

    // Plugin jQuery
    $.previousPicture = previousPicture;
    $.nextPicture = nextPicture;
})(jQuery);

(function($) {
    var currentProject = {},
        projectsWithPicturesLoaded = [],
        pictureShown = 1,
        numImages = 0,
        numProjects = $(".projectOverview").length,
        moving = false,
        projectListHeight,
        heightSave = {},
        $projectContent = $('#projectContent'),
        $pannelView = $('#pannelView'),
        initialProjectContentHeight = $projectContent.height(),
        pictureZindex = 5,
        isAnimated = false; // Lock during animation

    /**
     * Add or remove a circle button to match the number passed in parameter
     * @param {integer} numButtons Number of button to display
     */
    var addOrRemoveCircleButton = function(numButtons) {
        var $previewButtons = $('#previewButtons'),
            currentNumButton = $previewButtons.children().length-2;

        if (currentNumButton < numButtons) {
            var $div;
            for (var i=currentNumButton+1; i<=numButtons; ++i) {
                $div = $('<div class="circleButton" id="c' + i + '"></div>');
                if (i===1) {
                    $div.addClass('selected');
                }
                $previewButtons.find('.rightArrow').before($div);

                $div.on('click', {numCircle: i}, function(e) {
                    showPicture(e.data.numCircle);
                });
            }
        } else {
            for (var j=currentNumButton; j>numButtons; --j) {
                $("#previewButtons #c"+j).remove();
            }
        }
        if (numButtons > 1) {
            $previewButtons.height('80px');
        } else {
            $previewButtons.height('');
        }
    };

    /**
     * Thumbnails handling
     */
    $('.projectImagePreview').on('click', function() {
        if (isAnimated) {return '';}
        var self          = $(this),
            id            = self.attr('id').match(/project(\d+)/)[1],
            showCircles   = 0,
            imagesURL     = [],
            positionSave  = [],
            showThumbnail = new TimelineLite({
                onComplete: function() {
                    showCircles++;
                    self.trigger('startDisplayCircles');
                }
            });
        if (id === currentProject.id) {return '';}
        isAnimated = true;
        projectListHeight = $('#thumbnails').height();
        currentProject.top = self.position().top + 'px';
        currentProject.left = self.position().left + 'px';
        currentProject.width = self.width();
        currentProject.margin = self.css('margin');
        currentProject.preview = self;
        currentProject.id = id;

        // Make sure that both timeout and ajax query are done
        self.on('startDisplayCircles', function() {
            if (showCircles === 2) {
                imagesLoaded(self, numImages, imagesURL);
            }
        });

        // Get pictures linked to the project
        if (projectsWithPicturesLoaded.indexOf(id) === -1) {
            $.ajax({
                  url: data.getSiteImages+'/'+id,
                  type: 'GET',
                  dataType: 'json'
                })
                .done(function(json) {
                    showCircles++;
                    numImages = json.nbImages;
                    imagesURL = json.urls;
                    for (var i=0, l=numImages; i<l; ++i) {
                        $('#project'+id+' .largePictures').append(
                            $('<img/>')
                                .attr('src', data.baseURLImages+imagesURL[i].url)
                                .addClass('additionalPicture')
                            );
                    }
                    projectsWithPicturesLoaded.push(id);
                    // Add the first picture in count
                    numImages++;
                    self.trigger('startDisplayCircles');
                })
                .fail(function() {
                    console.log('Didn\'t retrieve the pictures');
                    self.trigger('startDisplayCircles');
                });
        } else {
            numImages = $('#project'+id+' .largePictures img').length;
            showCircles++;
            self.trigger('startDisplayCircles');
        }

        $projectContent.height($projectContent.height()+'px');
        $('.projectImagePreview').each(function() {
            var top = $(this).position().top,
                left = $(this).position().left;
            positionSave.push({'top': top, 'left': left});
        });
        $('.projectImagePreview').each(function(index) {
            $(this).css({
                'position': 'absolute',
                'top': positionSave[index].top + 'px',
                'left': positionSave[index].left + 'px',
                'margin': $(this).css('margin')
            });
        });
        // Hide the other thumbnails
        self.siblings().each(function() {
            $(this).addClass('hidden');
        });
        //TweenLite.to(self.siblings(), 1, {opacity: 0, scale: 0.6, ease: Cubic.easeInOut});

        setTimeout(function() {
            // Animate it to put it at the top
            self.addClass('active');
            showThumbnail.insert(TweenLite.to(self, 0.5, {
                top: 0,
                left: 0,
                width:'100%',
                margin:'0 auto 20px 0',
                ease: Cubic.easeInOut
            }), 0);
            showThumbnail.insert(TweenLite.to($projectContent, 0.5, {
                'margin-top': '50px',
                ease: Cubic.easeInOut
            }), 0);
            showThumbnail.insert(TweenLite.to('#backToList', 0.5, {
                opacity: 1,
                ease: Cubic.easeInOut
            }), 0);
        }, 1000);

        // When all the circles are shown
        self.one('lastCircleShown', function() {
            var $leftDiv = $('#content'+id+' .oneQuarterCard'),
                $rightDiv = $('#content'+id+' .threeQuarterCard'),
                newHeight = 0;

            $('#content'+id).show();
            $leftDiv.css('top', -$leftDiv.height()-100);
            $rightDiv.css('top', -$rightDiv.height()-$leftDiv.height()-100);
            TweenLite.to($leftDiv, 0.6, {
                top: '0',
                ease: Cubic.easeInOut
            });
            TweenLite.to($rightDiv, 0.6, {
                top: '0',
                ease: Cubic.easeInOut,
                onComplete: function() {
                    // Set height correctly
                    var $mainPicture = $('#project'+id+' .mainThumbnail');

                    newHeight += self.height();
                    newHeight += $('#previewButtons').height();
                    newHeight += $('#content'+id).height();
                    newHeight += 20;
                    $projectContent.height(newHeight);
                    $pannelView.css('height', $('#page2').height()+"px");
                    $('#project'+id).css('height', $('#project'+id+' .mainThumbnail').height());
                    $('#project'+id+' .mainThumbnail').css({'position': 'absolute', 'z-index': '10'});
                    isAnimated = false;
                    heightSave.pictureHeight = $('#project'+id+' .mainThumbnail').height();
                    heightSave.projectContentHeight = $projectContent.height();
                    heightSave.pannelViewHeight = $pannelView.height();
                }
            });
        });
    });

    /**
     * Action when the images are retrived
     */
    var imagesLoaded = function(self, numImages, imagesURL) {
        var $previewButtons = $('#previewButtons'),
            i = 0;

        // Update the number of rounds to display
        addOrRemoveCircleButton(numImages);
        self.css('position', '');
        self.siblings().each(function() {
            $(this).hide();
        });
        $previewButtons.show();
        if (numImages > 1) {
            TweenMax.staggerTo($previewButtons.children(), 0.3, {
                top:'50px',
            }, 0.08, function() {self.trigger('lastCircleShown');});

        } else {
            self.trigger('lastCircleShown');
        }
    };

    /**
     * Return to list view
     */
    $("#backToList").on('click', function() {
        if (isAnimated) {return '';}
        if (currentProject) {
            var $leftDiv      = $('#content'+currentProject.id+' .oneQuarterCard'),
                $rightDiv     = $('#content'+currentProject.id+' .threeQuarterCard'),
                hideContent = function() {
                    $('#project'+currentProject.id+' .mainThumbnail').css('position', '');
                    $('#project'+currentProject.id).css('height', '');
                    contentTL.insert(TweenLite.to($leftDiv, 0.6, {
                        top: -$leftDiv.height()-100,
                        ease: Cubic.easeInOut
                    }), 0);
                    contentTL.insert(TweenLite.to($rightDiv, 0.6, {
                        top: -$rightDiv.height()-$leftDiv.height()-100,
                        ease: Cubic.easeInOut
                    }), 0);
                    contentTL.insert(TweenLite.to('#backToList', 0.6, {
                        opacity: 0,
                        ease: Cubic.easeInOut
                    }), 0);
                    contentTL.insert(TweenLite.to($projectContent, 0.6, {
                        'margin-top': '10px',
                        ease: Cubic.easeInOut
                    }), 0);
                },
                buttonsHidden = function() {
                    // When content div are hidden, start to hide circular buttons
                    var $previewButtons = $('#previewButtons'),
                        numImages = $previewButtons.children().length,
                        i = numImages;

                    $('#content'+currentProject.id).css('display', '');

                    if (numImages > 1) {
                        TweenMax.staggerTo($previewButtons.children(), 0.2, {
                            top:'0',
                        }, 0.08, reduceToGridView);
                    } else {
                        reduceToGridView();
                    }
                },
                reduceToGridView = function() {
                    $('#project'+currentProject.id + ' .additionalPicture').css('display', 'none');
                    $('#project'+currentProject.id).css('height', '');
                    $projectContent.height(projectListHeight);
                    $('#previewButtons').css('display', '');

                    TweenLite.to(currentProject.preview, 0.5, {
                        top: currentProject.top,
                        left: currentProject.left,
                        width: currentProject.width,
                        position: 'absolute',
                        margin: currentProject.margin,
                        ease: Cubic.easeInOut,
                        onComplete: showOtherProjects
                    });
                    currentProject.preview.removeClass('active');
                },
                showOtherProjects = function() {
                    $projectContent.removeClass('active');
                    $pannelView.css('height', $('#page2').height());
                    currentProject.preview.siblings().each(function() {
                        $(this).css('display', '');
                        $(this).removeClass('hidden');
                    });
                    $('.projectImagePreview').each(function(index) {
                        $(this).css({
                            'position': '',
                            'top': '',
                            'left': '',
                            'margin': ''
                        });
                    });
                    currentProject.preview.off('startDisplayCircles');
                    currentProject = {};
                    isAnimated = false;
                },
                contentTL = new TimelineLite({
                    onComplete: buttonsHidden
                });

            isAnimated = true;
            // Back to the first picture
            showPicture(1, hideContent);
        }
    });

    /**
     * Picture handling
     */
    var showPicture = function(id, cb) {
        if (id != pictureShown) {
            var self = this,
                $currentPicture = $('#project'+currentProject.id+' .largePictures img:nth-child('+pictureShown+')'),
                $nextPicture = $('#project'+currentProject.id+' .largePictures img:nth-child('+id+')'),
                $currentCircle = $('#c'+pictureShown),
                $nextCircle = $('#c'+id),
                nextPictureHeight = $nextPicture.height() > 300 ? 300 : $nextPicture.height(),
                newProjectContentHeight = $projectContent.height(),
                newPannelViewHeight = $pannelView.height(),
                afterAnimation = function() {
                    cb && (cb.call(self));
                };

            pictureShown = id;
            pictureHeightDifference = nextPictureHeight - heightSave.pictureHeight;
            newProjectContentHeight = heightSave.projectContentHeight + pictureHeightDifference;
            newPannelViewHeight = heightSave.pannelViewHeight + pictureHeightDifference;
            if (pictureZindex >= 49) {
                pictureZindex = 5;
                $('#project'+currentProject.id+' .largePictures img').css('z-index', 4);
            } else {
                pictureZindex += 2;
            }
            $nextPicture.css({'display': 'block', 'z-index': pictureZindex+1, 'opacity': 0});
            $currentPicture.css({'display': 'block', 'z-index': pictureZindex});
            $nextCircle.addClass('selected');
            $currentCircle.removeClass('selected');
            pictureShown = id;
            TweenLite.to($nextPicture, 0.6, {
                opacity: 1,
                ease: Cubic.easeInOut,
                onComplete: afterAnimation
            });
            TweenLite.to($('#project'+currentProject.id), 0.6, {
                height: $nextPicture.height(),
                ease: Cubic.easeInOut,
            });
            TweenLite.to($projectContent, 0.6, {
                height: newProjectContentHeight,
                ease: Cubic.easeInOut,
            });
            TweenLite.to($pannelView, 0.6, {
                height: newPannelViewHeight,
                ease: Cubic.easeInOut,
            });
        } else {
            cb && (cb.call(this));
        }
    };

    var nextProjectPicture = function() {
        if (currentProject.id && !isAnimated) {
            var nextPhotoId = (pictureShown === numImages) ? 1 : pictureShown+1;
            showPicture(nextPhotoId);
        }
    };

    var previousProjectPicture = function() {
        if (currentProject.id && !isAnimated) {
            var previousPhotoId = (pictureShown === 1) ? numImages : pictureShown-1;
            showPicture(previousPhotoId);
        }
    };

    $('#previewButtons .leftArrow').on('click', function() {
        previousProjectPicture();
    });
    $('#previewButtons .rightArrow').on('click', function() {
        nextProjectPicture();
    });

    // Plugin jQuery
    $.previousProjectPicture = previousProjectPicture;
    $.nextProjectPicture = nextProjectPicture;
    /**
     * Actions to perform on window resize
     **/
    $.resizeAction = function() {
        // If a project is shown
        if (currentProject.hasOwnProperty('id')) {
            // Correct #projectX and #projectContent height
            var newHeight = $('#project'+currentProject.id+' .largePictures img:nth-child('+pictureShown+')').height(),
                newProjectContentHeight = 0;
            newHeight = newHeight > 300 ? 300 : newHeight;
            newProjectContentHeight = newHeight;
            $('#project'+currentProject.id).height(newHeight);
            newProjectContentHeight += $('#previewButtons').height();
            newProjectContentHeight += $('#content'+currentProject.id).height();
            newProjectContentHeight += 20;
            $projectContent.height(newProjectContentHeight);
            $('#pannelView').height($('#page2').height());
            heightSave.pictureHeight = $('#project'+currentProject.id+' .mainThumbnail').height();
            heightSave.projectContentHeight = newProjectContentHeight;
            heightSave.pannelViewHeight = $pannelView.height();
        } else {
            $projectContent.height($('#thumbnails').height());
        }
    };
})(jQuery);
