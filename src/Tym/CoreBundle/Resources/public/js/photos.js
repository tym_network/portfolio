(function($) {
    'use strict';
    var totPictures    = $(".picture").length,
        currentPhotoId = 1,
        previousId;

    /**
     * Fill the canvas with a blurry version of the pictures and display the first picture
     */
    var initialize = function() {
        var $pictures = $(".picture"),
            $canvas = $(".canvas"),
            $zoom = $(".zoom");

        // Display first picture
        $pictures.first().css({
          "opacity": "1",
          "display": "block"
        });
        $canvas.first().css("opacity", 1);
        $zoom.first().css("display", "block");
        // Add blurred canvas
        $pictures.each(function() {
            var num = $(this).attr("id").replace("pic", ""),
                canvas = document.getElementById("blurPic"+num),
                picture = this;

            canvas.getContext("2d").drawImage(picture, 0, 0);
            stackBlurImage($(this).attr("id"), "blurPic"+num, 40, false);
        });
    };

    /**
     * Display a specific picture
     */
    var showPicture = function(id) {
        previousId = currentPhotoId;
        currentPhotoId = id;
        var $oldPicture = $("#pic"+previousId),
            $oldZoom = $("#zoom"+previousId),
            $otherCanvas = $(".canvas"),
            $oldRound = $("#round"+previousId),
            $newPicture = $("#pic"+currentPhotoId),
            $newZoom = $("#zoom"+currentPhotoId),
            $newCanvas = $("#blurPic"+currentPhotoId),
            $newRound = $("#round"+currentPhotoId);

        $oldPicture.animate({"opacity": "0"}, 500, "linear", function() {
            $oldZoom.css("display", "");
            if (currentPhotoId==id) {
                $newZoom.css("display", "block");
                $newPicture.animate({"opacity": "1"}, 1000, "linear", function() {
                    $otherCanvas.css("opacity", 0);
                    $newCanvas.css("opacity", 1);
                });
            }
        });
        $oldRound.removeClass("selected");
        $newRound.addClass("selected");
    };

    /**
     * Display the next picture
     */
    var nextPicture = function() {
        var nextPhotoId = (currentPhotoId === totPictures) ? 1 : currentPhotoId+1;
        showPicture(nextPhotoId);
    };

    /**
     * Display the previous picture
     */
    var previousPicture = function() {
        var previousPhotoId = (currentPhotoId === 1) ? totPictures : currentPhotoId-1;
        showPicture(previousPhotoId);
    };

    $(window).load(function() {
        initialize();
    });

    $("#rightArrow").click(function() {
        nextPicture();
    });

    $("#leftArrow").click(function() {
        previousPicture();
    });

    $(".photos .circleButton").click(function() {
        showPicture(parseInt($(this).attr("id").replace("round","")));
    });

    // Plugin jQuery
    $.previousPicture = previousPicture;
    $.nextPicture = nextPicture;
})(jQuery);
