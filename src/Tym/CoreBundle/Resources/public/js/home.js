function initialize() {
    var map_canvas = document.getElementById('map_canvas');
    var map_options = {
        center: new google.maps.LatLng(48.8373, 2.2413),
        zoom: 13,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    }
    var map = new google.maps.Map(map_canvas, map_options);
    var latLng = new google.maps.LatLng(48.8373, 2.2413);
    var marker = new google.maps.Marker({
        position: latLng,
        map: map,
    });
}

if (google && google.maps) {
    google.maps.event.addDomListener(window, 'load', initialize);
}