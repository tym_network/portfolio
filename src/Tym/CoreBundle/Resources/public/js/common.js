(function($) {
    var currentPageShown = 1,
        pageMatrix = ['#home', '#projects', '#photos', '#biography', '#contact'];

    $(window).load(function () {
        $('#links').css('height', '0');
        displayCorrectPage();
        correctCanvasHeight();
    });

    /**
     * When the window is resized, adapt the height of the content.
     */
    $(window).resize(function () {
        if (currentPageShown === 2) {
            $.resizeAction();
        }

        var currentPage = $('#page'+currentPageShown);
        setPageHeight(currentPage.css('height'));
        correctCanvasHeight();
    });

    /**
     * Change the height of the canvas to fit the picture.
     */
    var correctCanvasHeight = function() {
        $('#photoViewer').css('height', $('#pic1').css('height'));
        $('.canvas').css({
            'width': $('#photoViewer').css('width'),
            'height': 'auto',
        });
    };

    /**
     * Set the size of the page so that is covers at least the whole page.
     */
    var setPageHeight = function(h) {
        var pannelView = $('#pannelView');
        var min = $(window).height()-($('#footer').height()+$('#header').height()+parseInt($('#footer').css('marginTop').replace('px',''))+parseInt($('#footer').css('marginBottom').replace('px','')));
        if (h.replace('px','') > min) {
            pannelView.css('height', h);
        } else {
            pannelView.css('height', min+'px');
        }
    };

    /**
     * Redirect to the right page depending on the hash tag.
     */
    var displayCorrectPage = function() {
        var pageAsked = jQuery.inArray($(location).attr('hash'), pageMatrix)+1;
        if (!pageAsked) {
            pageAsked = 1;
        }
        initiate(pageAsked);
    };

    /**
     * Display the page with the id number 'id'
     */
    var moveToContent = function(id) {
        if (id != currentPageShown) {
            var $currentPage = $('#page'+currentPageShown),
                $newPage = $('#page'+id);

            $newPage.css('transition', '');
            if (id > currentPageShown) {
                // Display a page that is after the current one
                $currentPage.css('transition', 'transform .6s cubic-bezier(1,0,0,1)');
                $currentPage.css('transform', 'translateX(-100vw)');
                $newPage.css('transform', 'translateX(100vw)');
            } else if (id < currentPageShown) {
                // Display a page that is before the current one
                $currentPage.css('transition', 'transform .6s cubic-bezier(1,0,0,1)');
                $currentPage.css('transform', 'translateX(100vw)');
                $newPage.css('transform', 'translateX(-100vw)');
            }
            setTimeout(function() {
                $newPage.css('transition', 'transform .6s cubic-bezier(1,0,0,1)');
                $newPage.css('transform', 'translateX(0)');
            });
            $('#links li').eq(id-1).addClass('current');
            $('#links li').eq(currentPageShown-1).removeClass('current');
            setPageHeight($newPage.css('height'));

            setKeyboardShortcuts(currentPageShown, id);

            currentPageShown = id;
            ga('send', 'pageview', {
              'page': pageMatrix[id],
              'title': pageMatrix[id]
            });
        }
    };

    /**
     * Use mousetrap to set keyboard shortcuts (switch between pictures)
     *
     * @param {integer} previousId - ID of the page that will be hidden
     * @param {integer} nextId - ID of the page that will be shown
     */
    var setKeyboardShortcuts = function(previousId, nextId) {
        // Remove Listeners for projects and pictures
        if (previousId === 2 || previousId === 3) {
            Mousetrap.unbind('right');
            Mousetrap.unbind('left');
        }
        if (nextId === 2) {
            // For projects
            Mousetrap.bind('right', function() { $.nextProjectPicture(); });
            Mousetrap.bind('left', function() { $.previousProjectPicture(); });
        }
        else if (nextId === 3) {
            // For pictures
            Mousetrap.bind('right', function() { $.nextPicture(); });
            Mousetrap.bind('left', function() { $.previousPicture(); });
        }
    };

    var initiate = function(page) {
        var $newPage = $('#page' + page);
        currentPageShown = page;
        $newPage.css('transform', 'translateX(0)');
        $('#links li').eq(page-1).addClass('current');
        setPageHeight($newPage.css('height'));
    };

    $('#links a').click(function() {
        linkClicked($(this).parent().index()+1);
    });

    $('#logo').click(function() {
        linkClicked(1);
    });

    $('#siteTitle').click(function() {
        linkClicked(1);
    });

    var linkClicked = function(id) {
        moveToContent(id);
        if (navIsShown) {
            $('#links').css('height', '0');
            navIsShown = false;
        }
    };

    /**
     * For the nav in Smartphone mode
     */

    var navIsShown = false;

    $('#menuButtonSmartphone').click(function() {
        if (navIsShown) {
            $('#links').css('height', '0');
            navIsShown = false;
        } else {
            $('#links').css('height', '');
            navIsShown = true;
        }
    });


    /**
     * Hide heaband when clicking on close button
     */
    var $headbandClose = $('.headband-close');
    $headbandClose.click(function() {
        $('.headband').remove();

        // Set cookie
        document.cookie = 'hide-banner=1; path=/';
    });
})(jQuery);
