<?php

namespace Tym\CoreBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Tym\CoreBundle\Entity\Message;
use Tym\CoreBundle\Form\MessageType;

class CoreController extends Controller
{
    public function indexAction()
    {
        $age  = date('Y') - 1991;
        if(date('m')<9 ||(date('m')==9 &&  date('j')<=1)){
            $age--;
        }

        $message = new Message();
        $form = $this->createForm(new MessageType(), $message);
        $messageSent = false;

        $request = $this->get('request');
        if ($request->getMethod() == 'POST') {
            $form->bind($request);
            $message->getRecaptcha();
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($message);
                $em->flush();
                $messageSent = true;
                mail('contact@theotimeloiseau.com', 'Nouveau Message sur Ty\'m', $message->getNom().' a envoyé le message suivant :
                '.$message->getContenu());
            }
        }

        // Use the locale of the user (Accept-language in HTTP Request) or, if it exists, use the one is the URL.
        $locale = $request->getLocale();
        if (strpos($request->getUri(), $locale) === false) {
            $userLocales = $request->getLanguages();
            $firstUserLocales = substr(strval($userLocales[0]),0,2);
            if ($firstUserLocales=="fr" || $firstUserLocales=="en") {
                $locale = $firstUserLocales;
            }
            $request->setLocale($locale);
        }

        if ($locale === 'en') {
            $resumeName = "LoiseauTheotime-Resume";
        } else {
            $resumeName = "LoiseauTheotime-C.V";
        }

        $repository = $this->getDoctrine()->getManager()->getRepository('TymCoreBundle:Site');
        $sites = $repository->getSitesAvecMiniatures();

        $repository = $this->getDoctrine()->getManager()->getRepository('TymCoreBundle:Photo');
        $photos = $repository->findBy(array(),array('ordre' => 'asc'));

        // Redirect permet de savoir si l'on se trouve sur la page "accueil" : si on clique sur un lien du header, il ne faut pas recharger la page mais seulement faire "glisser" le contenu
        // L'utilisation d'ancres (#h,#g,#b,#c) permet, lors d'un refresh de revenir à la page adéquate.
        // La variable page permet de charger la bonne page lorsque l'on vient d'une page différente (ayant nécessité un rechargement)
        return $this->render('TymCoreBundle:Core:index.html.twig', array('age' => $age,'form' => $form->createView(), 'messageSent' => $messageSent, 'sites' => $sites, 'photos' => $photos, 'locale' => $locale, 'resumeName' => $resumeName));
    }

    public function viewSitesAction($id)
    {
        if ($id != null || $id === -1) {
            $repository = $this->getDoctrine()->getManager()->getRepository('TymCoreBundle:ImagesSite');
            $images = $this->getDoctrine()->getManager()
                ->createQuery(
                    'SELECT i.url
                    FROM TymCoreBundle:ImagesSite i
                    WHERE i.site = :site
                    ORDER BY i.ordre ASC'
                )
                ->setParameter('site', $id)
                ->getResult();
            $data = array('urls' => $images, 'nbImages' => count($images));
            $response = new Response(json_encode($data));
            $response->headers->set('Content-Type', 'application/json');
            return $response;
        } else {
            $response = new Response("{}");
            $response->headers->set('Content-Type', 'application/json');
            return $response;
        }
    }
}
