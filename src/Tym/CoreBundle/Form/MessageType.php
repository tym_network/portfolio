<?php

namespace Tym\CoreBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\Collection;
use EWZ\Bundle\RecaptchaBundle\Form\Type\EWZRecaptchaType;

class MessageType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nom', 'text')
            ->add('mail', 'email')
            ->add('contenu', 'textarea')
            ->add('recaptcha', EWZRecaptchaType::class, array(
                'attr' => array(
                    'options' => array(
                        'theme' => 'light',
                        'type'  => 'image',
                        'size'  => 'normal',
                        'defer' => true,
                        'async' => true,
                    )
                )
            ));
    }

    public function getName()
    {
        return 'message';
    }

    public function getDefaultOptions(array $options)
    {
        return array(
            'data_class' => 'Tym\CoreBundle\Entity\Message',
        );
    }
}