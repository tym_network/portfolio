<?php

namespace Mozilla\MainBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('MozillaMainBundle:Default:index.html.twig', array());
    }
}
