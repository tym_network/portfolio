// Global variables
var windowWidth;
var windowHeight;
var row1Animated = false;
var row2Animated = false;
var row3Animated = false;

$().ready(function () {
  windowWidth = $(window).width();
  windowHeight = $(window).height();
  $("section h1 #secondPart").css("opacity",0);
  $("section #aboutMozilla").css("left", $(window).width());
  $("#mozillaFirefox .wrapContent").css("top", "100px");
});

// Settings the actions to the top links
$("#mozillaLink").click(function() {
  $('html,body').animate({scrollTop: $("#mozillaIntroduction").offset().top}, 500);
});

$("#firefoxLink").click(function() {
  $('html,body').animate({scrollTop: $("#mozillaFirefox").offset().top}, 1000);
});

$("#firefoxOSLink").click(function() {
  $('html,body').animate({scrollTop: $("#firefoxOS").offset().top}, 2000);
});

$("#otherLink").click(function() {
  $('html,body').animate({scrollTop: $("#otherProducts").offset().top}, 3000);
});

// Handles the opacity change while the user is scrolling
// obj = object which opacity changes; start = height (in px) at which the opacity starts to change; height = number of pixel during which the opacity should change; reference = specifies an object so that the animation starts refers to the number of pixels from the top of this object.
var changeOpacityWithScroll = function (obj, start, height, reference) {
  var scroll = $(window).scrollTop();
  var startY = start;
  var numerator = 0;
  if (reference !== undefined) {
    startY += reference.offset().top;
  }
  if (scroll >= startY && scroll <= startY+height) {
    numerator = (scroll-startY);
    numerator = numerator>0?numerator:0;
    obj.css("opacity", numerator/height);
  } else if (scroll < startY) {
    obj.css("opacity", 0);
  } else {
    obj.css("opacity", 1);
  }
}

// Changes the "left" css attribute while the user is scrolling
// obj = object which left attribute changes; start = height (in px) at which the left attribute starts to change; height = number of pixel during which the left attribute should change; reference = specifies an object so that the animation starts refers to the number of pixels from the top of this object.
var changeLeftWithScroll = function (obj, start, height, reference, leftInit, leftEnd) {
  var scroll = $(window).scrollTop();
  var startY = start;
  
  if (reference !== undefined && reference !== null) {
    startY += reference.offset().top;
  }
  if (scroll >= startY && scroll <= startY+height) {
    if (leftInit>leftEnd) {
      obj.css("left", leftInit-(scroll-startY)*Math.abs(leftEnd-leftInit)/height+"px");
    } else {
      numerator = leftInit+(scroll-startY);
      obj.css("left", leftInit+(scroll-startY)*Math.abs(leftEnd-leftInit)/height+"px");
    }
  } else if (scroll < startY) {
    obj.css("left", leftInit+"px");
  } else {
    obj.css("left", leftEnd+"px");
  }
}

// Change an object from fixed positioning to relative positioning, updating the "top" attribute at the same time
// obj = object which position should be changed; from = number of pixel at which the change should be done; topRelative = the value of the "top" attribute when the object is relatively positionned; topFixed = the value of the "top" attribute when the object is fixed positionned. 
var fixedToRelative = function (obj, from, topRelative, topFixed) {
  var scroll = $(window).scrollTop();
  if (scroll > from) {
    obj.css("position", "relative");
    obj.css("top", topRelative+"px");
  } else {
    obj.css("position", "fixed");
    obj.css("top", topFixed+"px");
  }
}

// Change an object from fixed positioning to absolute positioning, updating the "top" attribute at the same time
// obj = object which position should be changed; from = number of pixel at which the change should be done; topAbsolute = the value of the "top" attribute when the object is absolutely positionned; topFixed = the value of the "top" attribute when the object is fixed positionned. 
var fixedToAbsolute = function (obj, from, topAbsolute, topFixed) {
  var scroll = $(window).scrollTop();
  if (scroll > from) {
    obj.css("position", "absolute");
    obj.css("top", topAbsolute+"px");
  } else if (scroll <= from) {
    obj.css("position", "fixed");
    obj.css("top", topFixed+"px");
  }
}

// Change an object from relative positioning to fixed positioning, updating the "top" attribute at the same time
// obj = object which position should be changed; from = number of pixel at which the change should be done; topRelative = the value of the "top" attribute when the object is relatively positionned; topFixed = the value of the "top" attribute when the object is fixed positionned. 
var relativeToFixed = function (obj, from, topRelative, topFixed) {
  var scroll = $(window).scrollTop();
  if (scroll > from) {
    obj.css("position", "fixed");
    obj.css("top", topFixed+"px");
  } else {
    obj.css("position", "relative");
    obj.css("top", topRelative+"px");
  }
}

// Change an object from fixed positioning to absolute positioning, updating the "top" attribute at the same time
// obj = object which position should be changed; from = number of pixel at which the change should be done; topAbsolute = the value of the "top" attribute when the object is absolutely positionned; topFixed = the value of the "top" attribute when the object is fixed positionned. 
var absoluteToFixed = function (obj, from, topAbsolute, topFixed) {
  var scroll = $(window).scrollTop();
  if (scroll > from) {
    obj.css("position", "fixed");
    obj.css("top", topFixed+"px");
  } else if (scroll <= from) {
    obj.css("position", "absolute");
    obj.css("top", topAbsolute+"px");
  }
}

// Display a "card" for Firefox OS (move to the left)
// obj = card to display; leftEnd = position to reach when animation is over
var showFirefoxOSCard = function (obj, leftEnd) {
  obj
    .stop()
    .show()
    .css("opacity", 1)
    .animate({left: leftEnd+"px"}, 500, "swing");
}

// Hide a "card" for Firefox OS (move to the right + opacity = 0)
// obj = card to hide
var hideFirefoxOSCard = function (obj) {
  obj
    .stop()
    .show()
    .animate({left: "580px", opacity: 0}, 500, "swing", function() { $(this).hide()});
}


$(window).scroll(function() {
  var scroll = $(window).scrollTop();
  
  // Section 1
  var mozillaIntroduction = $("section#mozillaIntroduction");
  
  changeOpacityWithScroll($("section h1 #secondPart"), 0, 100);
  
  // Position the title relatively after 150px
  // 150px (scroll) + 150px (top) - Header size (60px) = 240px
  fixedToRelative($("section#mozillaIntroduction h1"), 150, 240, 150);
  
  changeLeftWithScroll($("section #aboutMozilla"), 100, 300, null, $(window).width(), 0);
  
  
  // Section 2 (Firefox)
  var mozillaFirefox = $("section#mozillaFirefox");
  var firefoxWindow = $("section #firefoxWindow");
  var mozillaFeatures = $("section #mozillaFeatures");
  
  relativeToFixed($("#mozillaFirefox .wrapFloat"), 1060, 0, 100);
  if (scroll > 2300) {
    $("#mozillaFirefox .wrapFloat")
      .css("position","absolute")
      .css("top","0px");
  }
  
  changeOpacityWithScroll(firefoxWindow, 700, 200, mozillaIntroduction);
  changeLeftWithScroll(firefoxWindow, 700, 200, mozillaIntroduction, -200, 0);
  changeOpacityWithScroll($("#mozillaFeatures li:nth-of-type(1)"), 0, 100, mozillaFirefox);
  changeOpacityWithScroll($("#mozillaFeatures li:nth-of-type(2)"), 120, 100, mozillaFirefox);
  changeOpacityWithScroll($("#mozillaFeatures li:nth-of-type(3)"), 240, 100, mozillaFirefox);
  changeOpacityWithScroll($("#mozillaFeatures li:nth-of-type(4)"), 360, 100, mozillaFirefox);
  changeOpacityWithScroll($("#mozillaFeatures li:nth-of-type(5)"), 480, 100, mozillaFirefox);
  
  // Section 3 (Firefox OS)
  absoluteToFixed($("#phone"), 2162, 0, 100);
  if (scroll > 3500) {
    $("#phone")
      .css("position","absolute")
      .css("top","0px");
  }
  
  if (row1Animated === false && scroll >= 2470) {
    row1Animated = true;
    showFirefoxOSCard($("#lock"), 0);
    showFirefoxOSCard($("#call"), 160);
    showFirefoxOSCard($("#appgrid"), 320);
  } else if (row1Animated === true && scroll < 2470) {
    row1Animated = false;
    hideFirefoxOSCard($("#lock"));
    hideFirefoxOSCard($("#call"));
    hideFirefoxOSCard($("#appgrid"));
  }
  
  if (row2Animated === false && scroll >= 2740) {
    row2Animated = true;
    showFirefoxOSCard($("#firefox"), 0);
    showFirefoxOSCard($("#inbox"), 160);
    showFirefoxOSCard($("#gallery"), 320);
  } else if (row2Animated === true && scroll < 2740) {
    row2Animated = false;
    hideFirefoxOSCard($("#firefox"));
    hideFirefoxOSCard($("#inbox"));
    hideFirefoxOSCard($("#gallery"));
  }
  
  if (row3Animated === false && scroll >= 2990) {
    row3Animated = true;
    showFirefoxOSCard($("#radio"), 0);
    showFirefoxOSCard($("#clock"), 160);
    showFirefoxOSCard($("#calculator"), 320);
  } else if (row3Animated === true && scroll < 2990) {
    row3Animated = false;
    hideFirefoxOSCard($("#radio"));
    hideFirefoxOSCard($("#clock"));
    hideFirefoxOSCard($("#calculator"));
  }
  
  // Section 4 (Other products)
  absoluteToFixed($("#thunderbird"), 3535, 100, 150);
  absoluteToFixed($("#marketplace"), 3875, 440, 150);
  absoluteToFixed($("#persona"), 4215, 780, 150);
});